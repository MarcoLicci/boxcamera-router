'use strict';

var http = require('http'),
    httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({});

var redis = require('redis'),
    redisClient = redis.createClient({ host: '192.168.1.100' });

var url = require('url');

http.createServer(function (req, res) {
    var subdomains = removeDomain(req.headers.host.split(':')[0].split('.'));

    if (subdomains) {
        var subdomain = subdomains.pop();
        if (subdomain == 'camere') {
            var cameraDomain = subdomains.pop();
            resolveCameraDomain(cameraDomain, function (target) {
                serveProxy(req, res, target);
            });
        } else {
            res.end();
        }
    } else {
        res.end();
    }

}).listen(3000);

var removeDomain = function (domains) {
    domains.pop();
    domains.pop();
    return domains;
}

var serveProxy = function (request, response, target) {
    proxy.web(request, response, {
        target: 'http://' + target+ ':8765/'
    });
}

var resolveCameraDomain = function (domain, callback) {
    redisClient.get(domain, function (error, databaseReply) {
        if (!error && databaseReply) {
            callback(databaseReply);
        } else {
            console.log(error);
        }
    });
}

